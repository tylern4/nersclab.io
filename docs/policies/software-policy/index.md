# NERSC Software Support Policy

## Disallowed software

NERSC does not allow the use of:

- Classified or controlled military defense information
- Export controlled or [ITAR](https://www.pmddtc.state.gov/ddtc_public?id=ddtc_kb_article_page&sys_id=%2024d528fddbfc930044f9ff621f961987)
  codes or data
- Personally identifiable information
- Protected health information

Software meeting this definition is disallowed even if it is not explicitly listed 
as "Resticted".

Additionally, as stated in the "Security" tab of the
[ERCAP Request Form](https://www.nersc.gov/users/accounts/allocations/request-form/),
NERSC supports only open research intended to be published in open scientific
journals. Proprietary research is not allowed.

## Prohibited software

The following applications are prohibited at NERSC. This list may change at any
time without notice. The attempted use of such software on or to access NERSC 
resources may result in limits placed on your NERSC account, up to and 
including the disabling of your account. Please create a ticket in the 
[NERSC help desk](https://help.nersc.gov) if you need help identifying an 
alternative solution.

| Software Name   | Use                    | Reason Prohibited                                           | Effective Date |
|-----------------|------------------------|--------------------------------------------|----------|
| Gaussian        | Scientific software    | Licensing restrictions                                      | 2018/02/12      |
| Magic Wormhole  | File transfer          | Significant security risk; functionality violates NERSC AUP | 2024/03/27      |
| Warp Terminal   | Terminal emulator      | Significant security risk; functionality violates NERSC AUP | 2024/06/01      |
