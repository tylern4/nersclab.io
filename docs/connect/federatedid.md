# Federated Identity

## Introduction

Federated identity is the means of linking a person's identity
across multiple organizations. The basic concept is that NERSC
services will trust identities coming from another organization;
that the person logging in is who they say they are. This enables
Single Sign-On (SSO), which you may already be familiar with at
your home organization, however now an SSO session can extend to
NERSC services.

Federated identity functions like a master key: instead of needing
two different usernames and passwords to log in to NERSC and your
home institution, you need only the username and password from your
home institution to use NERSC web services. (SSH functions differently,
so for the time being users will still need to use their NERSC
identity to SSH to a compute resource.)

The Federated Identity service is currently in the pre-pilot phase.
The information on this page will be updated as the project progresses.
You can find some frequently asked questions below.

## Frequently Asked Questions (FAQ)

### About Federated Identity

**(Q) How will federated identity be used at NERSC?**

NERSC will be providing users with an additional option for logging
in to NERSC web services. Users from eligible organizations will
be able to link their home identity, and use their home login service
for logging in to NERSC web applications. When a user logs in to
NERSC using an external identity, we will map the federated identifier
to the internal NERSC identifier for authorizing access to files,
allocations, and other NERSC project resources. Once logged in, a
user will have identical access rights, regardless of whether they
used their home identity or local NERSC identity.

**(Q) What services at NERSC can I access with my home identity?**

The following systems currently support federated logins:

| System   | Base URL | Purpose |
|:---------|:---------|:--------|
| Iris     | [https://iris.nersc.gov](https://iris.nersc.gov)         | Managing NERSC account & banking information |
| Spin     | [https://rancher2.spin.nersc.gov](https://rancher2.spin.nersc.gov) | Creating & managing Spin container-based computing services |
| Helpdesk | [https://help.nersc.gov](https://help.nersc.gov)         | Submitting & managing help requests |
| ERCAP    | [https://ercap.nersc.gov](https://ercap.nersc.gov)       | Applying for NERSC project allocations |
| Jupyter  | [https://jupyter.nersc.gov](https://jupyter.nersc.gov).  | Using Jupyter notebooks |

Additional NERSC services will be added over time.

**(Q) How is this new service being rolled out?**

There are three phases of the rollout. We are currently in the
**second phase**.

-  During our **first phase**, NERSC users that also have a Lawrence
   Berkeley Lab account were enabled to use their @lbl.gov account
   to log in to NERSC.
-  During the current **second phase**, additional Department of Energy
   Laboratory login services will be eligible.
-  After we've completed these initial phases, and ensured that our
   processes are ready for the general population, in the third phase 
   users belonging to any eligible organization will be able to link 
   their home identity and use it to login at NERSC.

**(Q) What organizational logins can be used for logging in to NERSC
services?**

To be eligible for use at NERSC, your home organization's login
service (known as an "identity provider") must:

-  Be part of the [US InCommon Identity Federation](https://incommon.org/);
-  be part of the [Research and Scholarship
   (R&S)](https://refeds.org/research-and-scholarship) entity
   category.  This means that the identity provider will release
   attributes (such as a unique identifier, your name, and email
   address) that NERSC needs for our login process; and
-  [participate in the Security Incident Response Trust Framework
   for Federated Identity (Sirtfi)](https://refeds.org/sirtfi)

**(Q) What do I do if my home institution isn't eligible?**

Nothing will change for you. You will continue to log in to NERSC
with your current NERSC username and password.

You may also help your home organization understand that supporting
these identity standards will make it easier for their users to
participate in scientific collaborations, and not just at NERSC.

-  [CERN also requires that organizations be compliant with
   Sirtfi](https://cern.service-now.com/service-portal/Useful?id=kb_article&n=KB0004765)
-  The [NIH will begin requiring participation in the Research and
   Scholarship
   category](https://incommon.org/news/nih-to-begin-requiring-multifactor-authentication/)
   (and multi-factor authentication) for access to their services

### Connecting Your Identity with NERSC

**(Q) How do I link my home identity to my NERSC account?**

Once your organization is eligible, you will need to complete a
one-time process to register your home identity. You can manually
initiate this process one of two ways:

-  Follow this link: [Add an identity](https://login-proxy.nersc.gov/saml/unsolicited?authId=https%3A%2F%2Fshib.nersc.gov%2Fidp%2Fshibboleth&providerId=https%3A%2F%2Fcomanage.nersc.gov%2Fshibboleth&target=https%3A%2F%2Fcomanage.nersc.gov%2Fregistry%2Fco_petitions%2Fstart%2Fcoef%3A24)
-  In Iris, navigate to your "Profiles" tab, and click on the "Add
   Identity" link in the "External Identities" section of the page

You will then be guided through a process that securely associates your
home institution identity with your NERSC account via a series of logins:

1. First, you will be asked to authenticate with your NERSC credentials.
2. Next, you will be asked to choose the institution of the identity
   you want to register and asked to authenticate.
3. After a series of progress messages, you will see confirmation that
   registration completed, along with links to various systems at NERSC
   that are enabled for Federated Identity.

Note that during this process, if you are already logged in to NERSC
or your home institution, you might not be prompted for your credentials.
(Note "single sign-on").

Alternatively, if you choose your home institution during authentication
_before_ it is registered, the process will automatically initiate.
After authenticating to your home institution, you will see a message
that the identity can be linked to your NERSC account. Click "Link a New
Identity" to start the process, then continue with Step 1 above.
Note that _you will need to select your home institution again_ when
prompted.

**(Q) Why can't I find my home organization when I try to link accounts?**

The search box will find only organizations that [meet our
criteria](https://incommon.org/news/nih-to-begin-requiring-multifactor-authentication/).

**(Q) What if I change institutions?**

Assuming that your new institution is eligible, you can link your new home
identity as described above.

Even if your new home identity is not eligible to be used at NERSC,
your NERSC username and password will always work for accessing
NERSC resources.

To unlink an old identity, please [file a help ticket](https://help.nersc.gov) for assistance.

### Using Federated Identity at NERSC

**(Q) Why am I still being prompted for a NERSC one-time password
after I login with my home identity?**

NERSC still requires the use of multi-factor authentication (MFA)
for accessing NERSC services. If your home organization uses MFA,
and [signals the use of MFA](https://refeds.org/profile/mfa) to
NERSC, we will not prompt for the NERSC second factor. However, if
we don't receive that signal, you will be prompted to enter your
NERSC one-time code.

If you have logged in with MFA at your home institution, and believe
this prompt to be in error, please [contact NERSC](https://help.nersc.gov) 
and the appropriate IT staff at your home institution, and we will 
work with them to assure that the correct signal is being sent
and received.

![NERSC login page, one-time password prompt](images/shib-login02b.png){: height="410" }
{: align="center" }

**(Q) Can I use ssh with my home identity?**

Unfortunately the SSH protocol isn't compatible with our federated
identity solution. We are evaluating options, such as a web portal
which would allow you to conveniently download an ssh certificate
(similar to the one provided by the [sshproxy](mfa.md#sshproxy)
script or Windows executable) to facilitate the use of federated
logins for ssh access to NERSC.

**(Q) If I'm using my home identity exclusively, do I still need to
keep an up-to-date password and MFA token associated with my NERSC
username?**

For now, yes.
