# E4S 23.08 - Perlmutter

The `e4s-23.08` stack is built using the Spack branch [e4s-23.08](https://github.com/spack/spack/tree/e4s-23.08)
on Perlmutter using the *gcc*, *nvhpc*, and *cce* compilers. This stack can be loaded by running:

```
module load e4s/23.08
```

Once you load the module, you can list all spack environments by running

```shell
elvis@perlmutter> spack env list
==> 4 environments
    cce  cuda  gcc  nvhpc
```

In-order to access software, you are required to activate one of the spack environments. For instance to access the 
`cuda` environment, you can run the following

```shell
elvis@perlmutter> spack env activate cuda
```

You can confirm your active environment by running `spack env st`. If you have ran the previous command, you should see
the following:

```shell
elvis@perlmutter>spack env st
==> In environment cuda
```

You can query all packages by running `spack find` and load packages into your user environment via `spack load`.

## Breakdown of Installed Specs

Shown below is a breakdown of installed specs by environment. 

| Spack Environments | Compiler     | Root Specs | Implicit Specs | Installed Packages  | Required Modules                                    |
|--------------------|--------------|------------|----------------|---------------------|-----------------------------------------------------|
| `gcc`              | `gcc@12.3.0` | 78         | 230            | 307                 | `PrgEnv-gnu`, `gcc-native/12.3`                     |
| `cuda`             | `gcc@12.3.0` | 18         | 68             | 95                  | `PrgEnv-gnu`, `gcc-native/12.3`, `cudatoolkit/12.2` |
| `nvhpc`            | `nvhpc@23.9` | 5          | 6              | 12                  | `PrgEnv-nvhpc`, `nvhpc/23.9`                        |
| `cce`              | `cce@17.0.0` | 7          | 7              | 14                  | `PrgEnv-cray`, `cce/17.0.0`                         |
