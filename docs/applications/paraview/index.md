# ParaView

## Introduction

ParaView is an open-source, multi-platform data analysis and
visualization application. ParaView users can quickly build
visualizations to analyze their data using both qualitative and
quantitative techniques. The data exploration can be done
interactively in 3D or programmatically using ParaView's batch
processing capabilities. ParaView was developed to analyze extremely
large datasets using distributed memory computing resources and can be
run on supercomputers to analyze terascale datasets, as well as on
laptops for smaller data.

## Remote Visualization with ParaView

ParaView is a client-server application. The ParaView client (or
simply paraview) will run on your desktop while the server will run at
the remote supercomputing site. The following describes the steps you
will take to install ParaView on your desktop and configure it so that
you can launch parallel jobs on remote systems from within the
ParaView GUI. Running ParaView remotely in a client-server
configuration involves establishing an ssh tunnel to the login node,
launching the ParaView server, and connecting the server to the client
over the tunnel via a socket.

![ParaView Server on compute node links to ssh on the front end, which connects through firewalls to the ParaView Client on a workstation](images/rc-tunnel.png)

## Install the ParaView Client

The first step is to install ParaView. It is recommended that you use
the binaries provided by Kitware on your workstation matching the
NERSC installed version. This ensures client-server compatibility. The
version number that you install must identically match the version
installed at NERSC.

| System | ParaView Version | Status |
|--------|------------------|--------|
| Perlmutter | 5.12.0       | Recommeneded |
| Perlmutter | <5.12.0       | Deprecated |

Use the following link to download the correct
ParaView client binary version for your desktop
environment: [ParaView binaries](https://www.paraview.org/download/).

## Install Dependencies

In order to use ParaView remotely you will also need an ssh client and a
terminal program.
**Linux.** On Linux this functionality is provided by the ssh and xterm
programs. These may need to be installed on Linux systems using your distribution's package
manager, e.g. *apt* or *dnf*.
**Windows.** On Windows the ssh and terminal functions
are provided by the programs *plink.exe* and *cmd.exe*, of which only
*cmd.exe* will come pre-installed. The ssh client, *plink.exe*, needs to
be installed before using ParaView. This program can be installed
using the Windows installer from the
[PuTTY download site](https://www.chiark.greenend.org.uk/~sgtatham/putty/). The
PuTTY install folder needs to be added to the system path after the
install. This can be done in the Windows control panel, by opening
*Control Panel\System and Security\System* , opening the *Advanced
System Settings* page, and opening the *Environment Variables* dialog
box.
**MacOS.** On MacOS one may use the terminal provided by Apple and NERSC provided OSA
script to connect to NERSC. The OSA script is provided in the following section.
Alternatively on
MacOS one may connect to NERSC servers via the *Linux like* configuration once
[XQuartz](https://www.xquartz.org/index.html) is installed. *XQuartz* may be
installed on MacOS using *brew*.

## Install the NERSC server config

NERSC provides an integrated server configuration that enables "point
and click" ssh tunnel configuration, job configuration and submission,
enabling users to quickly launch the ParaView server on NERSC systems.

  1. Close any instances of ParaView running on your desktop.
  2. Download the appropriate NERSC server configurations to your
     laptop or desktop. You can click to download or follow the link
     to copy the contents and save as the corresponding the pvsc filename.
  3. Open the ParaView client and click the Connect button or from the
     File menu choose connect. In the choose a server dialog box click
     the load servers button and select the NERSC server config.

The NERSC server configurations will now show up in ParaView's connect
menu.

### Select The Appropriate Server Configuration From The Table

| Operation System | ParaView Configuration | Launch Script |
| -----------------|------------------------|---------------|
| Linux | [perlmutter-unix.pvsc](pvsc/perlmutter-unix.pvsc) | none |
| Windows | [perlmutter-win.pvsc](pvsc/perlmutter-win.pvsc) | none |
| MacOS | [perlmutter-macos.pvsc](pvsc/perlmutter-macos.pvsc) | [perlmutter-macos.scpt](pvsc/perlmutter-macos.scpt) |

Note: The MacOS configuration requires an additional launch script which must
downloaded and saved locally with execute permissions. When connecting to NERSC
the first time through ParaView one must locate this file in the launch dialog.
This should only be necessary on the first connection. Alternatively one may
use the *Linux* configuration on MacOS if XQuartz (or some other X11
implementation) is installed.

## Connecting to the Server on NERSC Systems

The steps are summarized below.

1.  Download the configuration script from the table above that corresponds
    to your operating system.

    **For MacOS:** Download both the ``perlmutter-macos.pvsc`` and the
    launch script ``perlmutter-macos.scpt``.

    Make sure to give execute permissions to the script(s) by executing the
    ``chmod +x perlmutter-macos.pvsc`` and ``chmod +x perlmutter-macos.scpt``
     (for MacOS) commands in the terminal.

2.  Open ParaView and select **File->Connect** from the menu.

    ![ParaView main window with File menu in upper left](images/400px-Open-paraview.png){:width="300px"}

3.  Select **Load Servers** in the window that pops up.

    ![Select Load Servers](images/250px-Load-server.png){:width="300px"}

4.  Select the **pvsc** file in the pop-up window. Click **OK**.

    ![Choose server dialog with list of server names above and buttons below](images/250px-Choose-server.png){:width="300px"}

5.  Click **Connect** on the window in Step 3. Configure the run by setting
    the username, account, architecture (cpu/gpu), number of processes, processes
    per socket, job wall time and QOS in the next pop-up window. Click **OK**.

    **For MacOS:** specify the path and name of the launch script,
    ``perlmutter-macos.scpt`` in the last field.

    One common issue for Mac users is that the
    ParaView client sometimes reports `"The process failed to start. Either
    the invoked program is missing, or you may have insufficient permissions
    to invoke the program."`. To resolve this, make sure to give the full path 
    of the launch script - ``<path>/perlmutter-macos.scpt`` in the last field, 
    and give execute permissions to the configuration and launch (for MacOS) 
    scripts as given in Step 1.    

    ![ParaView server configuration dialog with labels on left and controls on right. Tunnel port is 11117](images/250px-Franklin-pv-server-config.png){:width="300px"}

6.  The Starting server dialog will be shown while the client waits for
the server to start and connect back.

    ![Please wait dialog with Cancel button](images/250px-Starting-server.png){:width="300px"}

7.  The server configuration script will start an xterm allowing you to login
    to the selected system. Enter the Password+OTP and login. Click the **Enter**
    button. Then you can see the that job has been submitted to the queue.
    Press **u** to see the status of the job. The tunnel is established between
    the workstation and the remote system once the job is allocated. This window needs
    to stay open while you use ParaView. See the image immediately below for
    a similar example.
    
    ![Terminal session with ParaView ascii art, last line is "Press: u to print job status or q to quit."](images/400px-Connecting.png){:width="300px"}

## Running in Batch Mode

In batch mode the ParaView server runs on its own without a client
executing a python script. Batch mode provides a way to automate
common visualization tasks and alleviates the necessity of being
present when the job executes. Log in to the desired system, and
submit your job using the batch script `pvbatch`.

For more information execute `pvbatch --help`
and an explanation of the required command line arguments
will be printed.

### Example ParaView Batch Python Script

The following ParaView batch script renders a sphere colored by MPI
Rank. It can be used for basic testing.

```python
from paraview.simple import *

s = Sphere()
s.ThetaResolution = 128
s.PhiResolution = 128

p = ProcessIdScalars()

rep = Show(p)
Render()

lr = p.PointData.GetArray("ProcessId").GetRange()
lut = GetLookupTableForArray("ProcessId",
         1,
	     RGBPoints=[lr[0], 0.0, 0.0, 1.0, lr[1], 1.0, 0.0, 0.0],
	     ColorSpace='Diverging',
	     VectorMode='Magnitude',
	     ScalarRangeInitialized=1.0)

rep.ColorArrayName = 'ProcessId'
rep.LookupTable = lut

Render()

WriteImage('pvbatch-test.png')
```

### See Also

ParaView home page. [http://www.ParaView.org](http://www.paraview.org/)
