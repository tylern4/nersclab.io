#include <sycl/sycl.hpp>
#include <oneapi/mkl/blas.hpp>

#include <cmath>
#include <iostream>

int main() {

  const int m = 600;
  const int n = 1200;
  const int p = 2400;

  std::vector<double> matA(m * n);
  std::vector<double> matB(n * p);
  std::vector<double> matC_serial(m * p);

  for (size_t i = 0; i < m; i++)
    for (size_t j = 0; j < n; j++)
      matA[i * n + j] = 1.0;

  for (size_t i = 0; i < n; i++)
    for (size_t j = 0; j < p; j++)
      matB[i * p + j] = 2.0;

  for (size_t i = 0; i < m; i++) {
    for (size_t j = 0; j < p; j++) {
      for (size_t d = 0; d < n; d++) {
        matC_serial[i * p + j] += matA[i * n + d] * matB[d * p + j];
      }
    }
  }

  sycl::queue q{sycl::gpu_selector_v};

  double *dev_a = sycl::malloc_device<double>((m * n), q);
  double *dev_b = sycl::malloc_device<double>((n * p), q);
  double *dev_c = sycl::malloc_device<double>((m * p), q);
  q.memcpy(dev_a, matA.data(), sizeof(double) * m * n);
  q.memcpy(dev_b, matB.data(), sizeof(double) * n * p);
  q.wait();

  oneapi::mkl::transpose transA = oneapi::mkl::transpose::nontrans;
  oneapi::mkl::transpose transB = oneapi::mkl::transpose::nontrans;
  oneapi::mkl::blas::column_major::gemm(q, transA, transB, p, m, n, 1.0, dev_b, p, dev_a, n, 0.0, dev_c, p);
  q.wait();

  std::vector<double> matC_parallel(m * p);
  q.memcpy(matC_parallel.data(), dev_c, sizeof(double) * m * p);
  q.wait();

  for (size_t i = 0; i < m; i++) {
    for (size_t j = 0; j < p; j++) {
      if (!(fabs(matC_parallel[i * p + j] - matC_serial[i * p + j]) <= 1.0e-8))
        return 1;
    }
  }

  sycl::free(dev_a, q);
  sycl::free(dev_b, q);
  sycl::free(dev_c, q);

  return 0;
}
