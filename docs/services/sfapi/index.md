# Superfacility API Documentation

This page presents some general information about the NERSC
Superfacility Application Programming Interface (API).

The API can be used to script jobs or workflows running against
NERSC systems. The goal is to support everything from a simple script
that submits a job to complex workflows that move files, check
job progress and make advance compute time reservations.

The API endpoint can be accessed here: https://api.nersc.gov/api/v1.2/

That is also the place to find the up-to-date reference documentation,
and it presents a UI to interact with the API from inside your browser.

## Getting started

The API provides both unauthenticated and authenticated endpoints. 
If, for example, you are only interested in querying the status of the 
systems via the unauthenticated  ``/status/`` endpoint group you are 
ready to go (see step 3).

If you'd like to use the the authenticated part of the API, 
for example, to submit jobs or to get accounting info you need to create
an SF API client in Iris first.
Please follow these steps.

1. [Create a Superfacility API Client in Iris.](authentication.md#client)     
   You can create the client on your 
   [profile page](https://iris.nersc.gov/profile) 
   in the section "Superfacility API Clients". The lifetime and IP 
   restrictions of the client depend on its
   [security level](authentication.md#policy): the more powerful a client, 
   the shorter its lifetime and the smaller
   the number of source IPs it can be used from. 
   
2. [Exchange a client credentials for access tokens.](authentication.md#exchange)     
   Use the private key of your client to generate a "client assertion" 
   which, in turn, can be exchanged for an "access token". 
   Access tokens have short lifetimes of approximately 10 minutes.

3. [Call the SuperFacility API (with the access token).](authentication.md#call)     
   Once you have the access token, you can call the API at:
   https://api.nersc.gov/api/v1.2/. 
   We also created a number of [examples](examples.md) for you to 
   get you started. Make sure to check them out.

## More Resources

An early preview 
([slides](https://drive.google.com/a/lbl.gov/file/d/13gEzb5El5W1WtQ7g7k2tfqInMxgrNgIX/view?usp=sharing), 
[video](https://drive.google.com/open?id=13mGwwo4YN5qVAGBOxYhLMC1IuGyB-cs6)) 
of version 1.0 of the API was presented 
at the [Superfacility](https://www.nersc.gov/research-and-development/superfacility/#toc-anchor-3)
demo series.

We have created a [Python client library](https://github.com/NERSC/sfapi_client) for the API which is
avaiable on [PyPI](https://pypi.org/project/sfapi-client/). See the
[documentation](https://nersc.github.io/sfapi_client/) for more details on how to use it.

## Citing the API

A paper specifically about the Superfacility API will be out soon. Some aspects of the API were first described in:

B. Enders et al.,"[Cross-facility science with the Superfacility Project at LBNL](https://ieeexplore.ieee.org/document/9307775)," 
2020 IEEE/ACM 2nd Annual Workshop on Extreme-scale Experiment-in-the-Loop Computing (XLOOP), 
2020, pp. 1-7, doi: 10.1109/XLOOP51963.2020.00006.
