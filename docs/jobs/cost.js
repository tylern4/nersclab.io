function calculate_cost()
{
    // get input values
    let arch = document.getElementById('arch').value;
    let nodes = document.getElementById('nodes').value;
    let qos_name = document.getElementById('qos').value;
    let time = document.getElementById('time').value;
    // calculate various parameters based on selections
    let preempt_cf = (arch == "cpu") ? 0.5 : (arch == "gpu") ? 0.25 : NaN;
    let max_nodes_arch = (arch == "cpu") ? 3072 : (arch == "gpu") ? 1536 : NaN;
    let qos_cf = 
        (qos_name == "premium") ? 2 :
        (qos_name == "debug_preempt") ? preempt_cf :
        1;
    let big_discount_factor =
        ((qos_name == "reservation") || (qos_name == "premium")) ? 1 :
        ((arch == "cpu" && nodes >= 256) || (arch == "gpu" && nodes >= 128)) ? 0.5 :
        1;
    let max_nodes = 
        (qos_name == "debug") ? 8 :
        (qos_name == "interactive" || qos_name == "jupyter") ? 4 :
        (qos_name == "preempt") ? 128 :
        (qos_name.includes("shared")) ? 0.5 :
        (qos_name == "debug_preempt") ? 2 :
        max_nodes_arch;
    let min_time =
        (qos_name == "preempt") ? 2 :
        0;
    let max_time = 
        (qos_name.includes("debug")) ? 0.5 :
        (qos_name.includes("interactive")) ? 4 :
        (qos_name == "jupyter") ? 6 :
        (qos_name == "reservation") ? Infinity :
        48;
    // get cost and check that job request is valid
    let cost = nodes * qos_cf * time * big_discount_factor;
    if (qos_name == "preempt") {
        if(time < min_time) {
            alert(`${time} hour(s) is too short for the ${qos_name} QOS; ${qos_name} jobs must be at least ${min_time} hours.`);
            cost = '';
        }
        else {
            cost = nodes * (2 + preempt_cf*(time - 2));
        }
    }
    if (nodes > max_nodes) {
        alert(`${nodes} nodes is too large for the ${qos_name} QOS; please select fewer nodes or a different QOS.`);
        cost = '';
    }
    if (time > max_time) {
        alert(`${time} hours is too long for the ${qos_name} QOS; please select a shorter time or a different QOS.`);
        cost = '';
    }
    document.getElementById('result').value = cost;
}