# Job Cost Calculator

This tool is to help calculate job costs, based on the values in our [queue policies](policy.md).

<script src = "cost.js"></script>

Node type:
<select id="arch">
<option value="cpu">CPU</option>
<option value="gpu">GPU</option>
</select>
<br>
QOS:
<select id = "qos">
<option value = "regular">regular</option>
<option value = "interactive">interactive</option>
<option value = "shared_interactive">shared_interactive</option>
<option value = "jupyter">jupyter</option>
<option value = "debug">debug</option>
<option value = "shared">shared</option>
<option value = "preempt">preempt</option>
<option value = "debug_preempt">debug_preempt</option>
<option value = "premium">premium</option>
<option value = "reservation">reservation</option>
</select>
<br>
Number of Nodes:
<input type="text" id="nodes" name="nodes" style="border: 1px solid black"/>
<br>
Time (hours):
<input type="text" id="time" name="time" style="border: 1px solid black"/>

<p>
<input type="button" style="border: 1px solid black" onclick="calculate_cost();" value="Calculate Job Cost">
<input type="text" id="result" style="border: 1px solid black" value=""> NERSC hours
</p>

Note that `premium` QOS jobs are calculated with a charge factor of 2 here; the
charge factor increases to 4 after a project has used over 20% of its
allocation on `premium` jobs.
